// Storyline 360
//---------------------------------------------
// Compatible with: Build v3.56
// Last updated: 25/10/21
// Notes: Any issues, speak with Gary / Ian C
// Version 3.0.0

console.log('---- STORYLINE 360 GRUNT ----');


/*----*/ /////// Required, don't change ///////
const options = {
    version: '2.2.0',
    root: 'Pub/',
    includes: '_includes/',
    js: 'js/',
    corelocation: "J:/Grunt Resources/SL3/_includes/",
    sample: 'sample/',
    scroll: 'hidden' //either set this to hidden or hidden auto
}

const g = Object.create(options);
/*----*/ /////// Required, don't change ///////




/*----*/ /////// PUBLISHED FOLDER NAME ////////
g.folder = 'Clinical Induction - Storyline output/'; // DONT FORGET TO INCLUDE THE '/' AT THE END OF THE FOLDER NAME
/*----*/ /////// PUBLISHED FOLDER NAME ////////

/*----*/ /////// MODULE TITLE ///////
g.title = 'Clinical Induction'; // ENSURE THAT THIS MATCHES THE COURSE TITLE IN STORYLINE
/*----*/ /////// MODULE TITLE ///////



/*----*/ /////// Edit the code below at your own risk, you break it, you bought it ///////

g.fullpath = g.root + g.folder;

g.samplepath = g.sample + g.folder;

g.jspath = g.includes + g.js;

var htmlversion = '<html data-dynamic-version="' + g.version + '"';

var jsscripts = `
<script src="` + g.jspath + `jquery-1.12.3.min.js"></script>
<script src="` + g.jspath + `jquery-ui.min.js"></script>
<script src="` + g.jspath + `bgJS.min.js"></script>
</html>`;

var resetJS = `
function SCORM_ResetStatus(){
  var existingStatus = SCORM_GetStatus();
  if(existingStatus == 1){
  } else if(existingStatus == 2){
  } else if(existingStatus == 3){
  } else {
    WriteToDebug("In SCORM_ResetStatus");
    SCORM_ClearErrorInfo();
    return SCORM_CallLMSSetValue("cmi.core.lesson_status", SCORM_INCOMPLETE);
  }
}

function brokenResetStatusFunction(){
`;

var analyticscode = `
<head class='wmApplied'>
<link href='/_includes/additions.css' rel='stylesheet' type='text/css'/>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create', 'UA-77854539-1', 'auto');ga('send', 'pageview');</script>
<!--[if IE]>
<style type='text/css'>.watermark {filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/_includes/img/watermark.png', sizingMethod='scale');background:none !important;}.watermark, .builtBy{-ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)' !important;filter: alpha(opacity=100) !important; opacity: 1 !important; display:none !important;</style><![endif]-->
`;

module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.registerTask("checkerrors", function() {
        var error = 0;

        //some initial options error checking
        var fs = require('fs');
        var corepath = g.corelocation;
        if (!fs.existsSync(corepath)) {
            console.log("Error: CORE GRUNT INCLUDES NOT FOUND: " + corepath);
            error++;
        } else {
            console.log('Core grunt files exist');
        }

        if (g.folder == "Folder name/") {
            console.log("Error: You haven't updated the folder name have you?");
            error++;
        } else {
            console.log("Folder name updated");
        }

        if (g.folder.indexOf('/') > -1) {
            console.log('Folder name has a / at the end');
        } else {
            console.log("Error: YOU HAVE FORGOTTEN TO PUT A / AT THE END OF YOUR FOLDER NAME");
            error++;
        }

        if (!fs.existsSync(g.fullpath)) {
            console.log("Error: Published folder doesn't exist at: " + g.fullpath);
            error++;
        } else {
            console.log('Published folder exists');
        }

        if (g.title == "Course name") {
            console.log("Error: You haven't updated the course title have you?");
            error++;
        } else {
            console.log("Course title updated");
        }

        var imagesjsonpath = g.includes + g.js + "images.json";
        if (!fs.existsSync(imagesjsonpath)) {
            console.log("Error: images.json doesn't exist at: " + imagesjsonpath);
            error++;
        } else {
            console.log('images.json exists');
        }

        var json = JSON.parse(fs.readFileSync(imagesjsonpath, 'utf8'));

        var objectKeysArray = Object.keys(json.images);
        var imgpatharr = [];
        objectKeysArray.forEach(function(objKey) {
            var imgsrc = json.images[objKey];

            imgpatharr.push(g.includes + 'images/' + imgsrc.src);
        })

        for (var i = 0, len = imgpatharr.length; i < len; i++) {
            var imgpath = imgpatharr[i];
            if (!fs.existsSync(imgpath)) {
                console.log("Error: Image doesn't exist at: " + imgpath);
                error++;
            } else {
                console.log('image exists');
            }
        }

        if (error) {
            if (error > 1) {
                var errordesc = '-- There are ' + error + ' errors --';
            } else {
                var errordesc = '-- There is ' + error + ' error --';
            }
            grunt.fail.fatal(errordesc);
        } else {
            console.log('No errors');
        }
    });

    grunt.initConfig({
        replace: {
            version: {
                src: [
                    g.fullpath + 'story.html',
                    g.fullpath + 'index_lms.html'
                ],
                overwrite: true,
                replacements: [{
                    from: '<html',
                    to: htmlversion
                }]
            },
            scripts: {
                src: [
                    g.fullpath + 'story.html',
                    g.fullpath + 'index_lms.html'
                ],
                overwrite: true,
                replacements: [{
                    from: '</html>',
                    to: jsscripts
                }]
            },
            configs: {
                src: [
                    g.fullpath + 'lms/scormdriver.js'
                ],
                overwrite: true,
                replacements: [{
                    from: 'var REVIEW_MODE_IS_READ_ONLY = true;',
                    to: 'var REVIEW_MODE_IS_READ_ONLY = false;'
                }]
            },
            resetstatus: {
                src: [
                    g.fullpath + 'lms/scormdriver.js'
                ],
                overwrite: true,
                replacements: [{
                    from: "function SCORM_ResetStatus(){",
                    to: resetJS
                }]
            },
            scrollFix: {
				src: [
					g.fullpath + 'html5/lib/stylesheets/desktop.min.css'
				],
				overwrite: true,
				replacements: [{
					from: 'body.view-desktop.has-scrollbar{overflow:scroll}',
					to: 'body.view-desktop.has-scrollbar{overflow:none}'
				}]
			},
            backgroundBodyID: {
                src: [
                    g.fullpath + 'story.html',
                    g.fullpath + 'index_lms.html'
                ],
                overwrite: true,
                replacements: [{
                    from: '<body',
                    to: '<body id="htmlBackground"'
                }]
            },
            coursetitle: {
                src: [
                    g.fullpath + g.includes + g.js + 'bgJS.min.js'
                ],
                overwrite: true,
                replacements: [{
                    from: '"dyn"',
                    to: '"' + g.title + '"'
                }]
            },
            /*analytics: {
                src: [
                    g.samplepath + 'story.html'
                    g.samplepath + 'index.html',
                    g.samplepath + 'Index.html',
                    g.samplepath + '*.html'
                ],
                overwrite: true,
                replacements: [{
                        from: 'HEAD>',
                        to: 'head>'
                    },
                    {
                        from: 'BODY',
                        to: 'body'
                    },
                    {
                        from: '<head>',
                        to: analyticscode
                    }
                ]
            },
            sample: {
                src: [
                    g.samplepath + 'story.html'
                ],
                overwrite: true,
                replacements: [{
                    from: "</body>",
                    to: `</body><script src='/_includes/js/additions.js' type='text/javascript'></script>`
                }]
            },*/
        },

        copy: {
            includes: {
                cwd: g.includes,
                src: ['**/*', '!**/*.db'],
                dest: g.root + g.folder + g.includes,
                expand: true
            },
            core: {
                cwd: g.corelocation,
                src: ['**/*', '!**/*.db'],
                dest: g.root + g.folder + g.includes,
                expand: true
            },
            sample: {
                cwd: 'pub/',
                src: ['**/*', '!**/*.db'],
                dest: 'sample/',
                expand: true
            },
            clientReplaceFiles: {
                cwd: g.fullpath,
                src: [
                    '*.html',
                    '!analytics-frame.html',
                    'lms/scormdriver.js',
                    'html5/lib/stylesheets/desktop.min.css',
                    '_includes/**',
                    '!**/*.db'
                ],
                dest: 'Source_files/Replace_publish_files/',
                expand: true
            },
            clientCopySource: {
                src: [
                    '*.story'
                ],
                dest: 'Source_files/',
                expand: true
            }
        }

    });

    grunt.registerTask('default', [
        'checkerrors',
        'copy:includes',
        // 'copy:core',
        'replace:version',
        'replace:scripts',
        'replace:configs',
        'replace:resetstatus',
        'replace:scrollFix',
        'replace:backgroundBodyID',
        'replace:coursetitle'
    ]);

    grunt.registerTask('sample', [
        'copy:sample',
        'replace:analytics',
        'replace:sample'
    ]);

    grunt.registerTask('client', [
        'copy:clientReplaceFiles',
        'copy:clientCopySource'
    ]);

};